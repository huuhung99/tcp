package model;

import java.io.Serializable;

public class CongNhan implements Serializable {
    private String id;
    private String hoTen;
    private QueQuan queQuan;
    private String gioiTinh;
    private int heSoLuong;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public QueQuan getQueQuan() {
        return queQuan;
    }

    public void setQueQuan(QueQuan queQuan) {
        this.queQuan = queQuan;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public int getHeSoLuong() {
        return heSoLuong;
    }

    public void setHeSoLuong(int heSoLuong) {
        this.heSoLuong = heSoLuong;
    }

    public CongNhan() {
    }

    public CongNhan(String id, String hoTen, QueQuan queQuan, String gioiTinh, int heSoLuong) {
        this.id = id;
        this.hoTen = hoTen;
        this.queQuan = queQuan;
        this.gioiTinh = gioiTinh;
        this.heSoLuong = heSoLuong;
    }

    @Override
    public String toString() {
        return "CongNhan{" +
                "id='" + id + '\'' +
                ", hoTen='" + hoTen + '\'' +
                ", queQuan=" + queQuan +
                ", gioiTinh='" + gioiTinh + '\'' +
                ", heSoLuong=" + heSoLuong +
                '}';
    }
}
