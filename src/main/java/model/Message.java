package model;

import java.io.Serializable;

public class Message implements Serializable {
    private int index;
    private Object data;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Message() {
    }

    public Message(int index, Object data) {
        this.index = index;
        this.data = data;
    }
}
