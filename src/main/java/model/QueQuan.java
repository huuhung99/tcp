package model;

import java.io.Serializable;

public class QueQuan implements Serializable {
    private int id;
    private String diaChi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public QueQuan() {
    }

    public QueQuan(int id, String diaChi) {
        this.id = id;
        this.diaChi = diaChi;
    }

    @Override
    public String toString() {
        return "quê:" +
                "id=" + id +
                ", diaChi='" + diaChi + '\'' +
                '}';
    }
}
