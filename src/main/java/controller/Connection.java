package controller;

import model.CongNhan;
import model.Message;
import model.QueQuan;
import view.ServerView;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Connection {
    private ServerView view;
    private java.sql.Connection con;
    private ServerSocket myServer;
    private Socket clientSocket;
    private int serverPort = 8888;

    public Connection(ServerView view) {
        this.view=view;
        getDBConnection("mysql", "root", "");
        view.showMessage("TCP server is running...");
        openServer(serverPort);
        while (true) {
            listenning();
        }
    }

    private void getDBConnection(String dbName, String username,
                                 String password) {

        String dbUrl = "jdbc:mysql://localhost:3306/" + dbName;
        String dbClass = "com.mysql.cj.jdbc.Driver";
        try {
            Class.forName(dbClass);
            con = DriverManager.getConnection(dbUrl,
                    username, password);

        } catch (Exception e) {
        }
    }

    private void openServer(int portNumber) {
        try {
            myServer = new ServerSocket(portNumber);
        } catch (IOException e) {
        }
    }

    private void listenning() {
        try {
            clientSocket = myServer.accept();
            ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
            ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
            Message o = (Message)ois.readObject();
            switch (o.getIndex()){
                case 1:
                    oos.writeObject(themCongNhan(o.getData()));
                    break;
                case 2:
                    oos.writeObject(timKiemCongNhanTheoTen(o.getData()));
                    break;
                case 3:
                    oos.writeObject(lietKeCongNhanCungMotTinh(o.getData()));
                    break;
                case 4:
                    oos.writeObject(layTatCaTinh());
                    break;
                default:break;
            }
        } catch (Exception e) {
        }
    }

    private Message layTatCaTinh() {
        Message message=new Message();
        List<QueQuan> queQuans=new ArrayList<>();
        String query = "Select * FROM quequan";
        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()){
                QueQuan queQuan=new QueQuan();
                queQuan.setId(Integer.valueOf(rs.getString("id")));
                queQuan.setDiaChi(rs.getString("dia_chi"));
                queQuans.add(queQuan);
            }
        } catch (Exception e) {
            message.setIndex(400);
            return message;
        }
        message.setData(queQuans);
        message.setIndex(200);
        return message;
    }

    private Message lietKeCongNhanCungMotTinh(Object data) {
        Message message=new Message();
        int id=(int) data;
        List<CongNhan> congNhans=new ArrayList<>();
        String query = "Select c.*,q.id as queQuanId,q.dia_chi diaChi FROM congnhan c " +
                "LEFT JOIN quequan q ON q.id=c.que_quan WHERE q.id=?";
        try {
            PreparedStatement pstmt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1,id);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()){
                CongNhan congNhan=new CongNhan();
                congNhan.setId(rs.getString("id"));
                congNhan.setHoTen(rs.getString("ho_ten"));
                congNhan.setGioiTinh(rs.getString("gioi_tinh"));
                congNhan.setHeSoLuong(rs.getInt("he_so_luong"));
                QueQuan queQuan=new QueQuan(rs.getInt("queQuanId"), rs.getString("diaChi"));
                congNhan.setQueQuan(queQuan);
                congNhans.add(congNhan);
            }
        } catch (Exception e) {
            message.setIndex(400);
            return message;
        }
        message.setData(congNhans);
        message.setIndex(200);
        return message;
    }

    private Message timKiemCongNhanTheoTen(Object data) {
        Message message=new Message();
        String ten=(String) data;
        List<CongNhan> congNhans=new ArrayList<>();
        String query = "Select c.*,q.id as queQuanId,q.dia_chi diaChi FROM congnhan c " +
                "LEFT JOIN quequan q ON q.id=c.que_quan WHERE c.ho_ten LIKE ?";
        try {
            PreparedStatement pstmt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1,"%"+ten+"%");
            ResultSet rs=pstmt.executeQuery();
            while (rs.next()){
                CongNhan congNhan=new CongNhan();
                congNhan.setId(rs.getString("id"));
                congNhan.setHoTen(rs.getString("ho_ten"));
                congNhan.setGioiTinh(rs.getString("gioi_tinh"));
                congNhan.setHeSoLuong(rs.getInt("he_so_luong"));
                QueQuan queQuan=new QueQuan(rs.getInt("queQuanId"), rs.getString("diaChi"));
                congNhan.setQueQuan(queQuan);
                congNhans.add(congNhan);
            }
        } catch (Exception e) {
            message.setIndex(400);
            return message;
        }
        message.setData(congNhans);
        message.setIndex(200);
        return message;
    }

    private Message themCongNhan(Object data) {
        Message message=new Message();
        String query = "INSERT into congnhan values(?,?,?,?,?)";
        try {
            CongNhan congNhan=(CongNhan) data;
            PreparedStatement pstmt = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, UUID.randomUUID().toString());
            pstmt.setString(2, congNhan.getHoTen());
            pstmt.setInt(3, congNhan.getQueQuan().getId());
            pstmt.setString(4, congNhan.getGioiTinh());
            pstmt.setInt(5,congNhan.getHeSoLuong());
            int i = pstmt.executeUpdate();
            if (i>0){
                message.setData("oke");
                message.setIndex(200);
            }
        } catch (Exception e) {
            message.setIndex(400);
            return message;
        }
        return message;
    }
}
