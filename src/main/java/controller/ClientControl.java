package controller;

import model.CongNhan;
import model.Message;
import model.QueQuan;
import view.ClientView;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;

public class ClientControl {
    private ClientView view;
    private String serverHost = "localhost";
    private int serverPort = 8888;

    public Message themCongNhan(CongNhan congNhan) throws Exception{
        return this.query(new Message(1,congNhan));
    }

    public Message timKiemCongNhan(String nextLine) throws Exception {
        return this.query(new Message(2,nextLine));
    }

    public Message thongKeCongNhanCungTinh(int id) throws Exception {
        return this.query(new Message(3,id));
    }

    public List<QueQuan> layQueQuan() throws Exception {
        Message query = this.query(new Message(4, null));
        return (List<QueQuan>) query.getData();
    }
    private Message query(Message message) throws Exception{

        Socket mySocket = new Socket(serverHost, serverPort);

        ObjectOutputStream oos = new ObjectOutputStream(mySocket.getOutputStream());

        oos.writeObject(message);
        ObjectInputStream ois = new ObjectInputStream(mySocket.getInputStream());

        message =(Message) ois.readObject();
        oos.close();
        ois.close();
        return message;
    }
}
