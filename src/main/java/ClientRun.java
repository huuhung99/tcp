import controller.ClientControl;
import controller.Connection;
import model.CongNhan;
import model.Message;
import model.QueQuan;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

public class ClientRun {
    public static void main(String[] args) throws Exception {
        Scanner sc=new Scanner(System.in);
        ClientControl clientControl=new ClientControl();
        while (true){
            System.out.println("1. Thêm công nhân\n2. Tìm kiếm công nhân gần đúng theo tên.\n3. Liệt kê công nhân cùng 1 tỉnh.");
            System.out.println("Mời bạn nhập:");
            int choice= Integer.valueOf(sc.nextLine());
            switch (choice){
                case 1:
                    CongNhan congNhan=new CongNhan();
                    System.out.println("Nhập tên công nhân:");
                    congNhan.setHoTen(sc.nextLine());
                    System.out.println("Nhập giới tính cho công nhân:");
                    congNhan.setGioiTinh(sc.nextLine());
                    System.out.println("---------quê quán--------");
                    List<QueQuan> queQuans =clientControl.layQueQuan();
                    for(QueQuan queQuan:queQuans)
                        System.out.println(queQuan);
                    System.out.println("Nhập id quê quán:");
                    int idQue=Integer.valueOf(sc.nextLine());
                    QueQuan queQuan = queQuans.stream().filter(q -> q.getId() == idQue).findFirst().get();
                    congNhan.setQueQuan(queQuan);
                    System.out.println("Nhập hệ số lương:");
                    congNhan.setHeSoLuong(Integer.valueOf(sc.nextLine()));
                    Message message=clientControl.themCongNhan(congNhan);
                    if(message.getIndex()==200)
                        System.out.println("Thêm thành công công nhân!");
                    else System.out.println("thất bại!");
                    break;
                case 2:
                    System.out.println("Nhập tên công nhân:");
                    Message message1=clientControl.timKiemCongNhan(sc.nextLine());
                    if(message1.getIndex()==200){
                        List<CongNhan> congNhans=(List<CongNhan>) message1.getData();
                        for(CongNhan c:congNhans){
                            System.out.println(c);
                        }
                    }
                    else System.out.println("thất bại!");
                    break;
                case 3:
                    System.out.println("---------quê quán--------");
                    List<QueQuan> queQuanss =clientControl.layQueQuan();
                    for(QueQuan queQuan1:queQuanss)
                        System.out.println(queQuan1);
                    System.out.println("Nhập id quê quán:");
                    int idQueQuan=Integer.valueOf(sc.nextLine());
                    Message message2=clientControl.thongKeCongNhanCungTinh(idQueQuan);
                    if(message2.getIndex()==200){
                        List<CongNhan> congNhans=(List<CongNhan>) message2.getData();
                        for(CongNhan c:congNhans){
                            System.out.println(c);
                        }
                    }
                    else System.out.println("thất bại!");
                    break;
            }
        }
    }
}
